import React from 'react'
import { Link } from 'react-router-dom';

export default class SimilarFilms extends React.Component {
    

    render() {
        const { slug, title, releaseYear } = this.props;

        return (
            <div>
                <Link to={"/film/" + slug}>{title}</Link>
                <p>{releaseYear}</p>
            </div>
        );
    }
}