import React from 'react'
import FilmPreview from '../FilmPreview/FilmPreview.js'

export default class FilmList extends React.Component {
    render() {
        const { films } = this.props;

        return (
            <section>
                {
                    films.map((film, i) => {
                        return (
                            <FilmPreview key={i} slug={film.slug} title={film.title} releaseYear={film.releaseYear} />
                        )
                    })
                }
            </section>
        );
    }
}