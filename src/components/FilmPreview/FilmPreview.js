import React from 'react';
import { Link } from 'react-router-dom';

export default class FilmPreview extends React.Component {
    render() {
        const { slug, title, releaseYear } = this.props;
        return (
            <Link to={'/film/' + slug}>
                <div class="card">
                    <h3>{title}</h3>
                    <p>{releaseYear}</p>
                </div>
            </Link>
        );
    }
}