import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Home from '../../pages/Home/Home';
import AppHeader from '../AppHeader/AppHeader';
import Film from '../../pages/Film/Film';
import About from '../../pages/About/About';

function App() {
  return (
    <Router>
      <AppHeader />

      <div className="App">
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/about" exact component={About} />
          <Route path="/film/:slug" exact component={Film} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
