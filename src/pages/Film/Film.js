import React from 'react'
import { getOneFilm } from '../../api.js';
import SimilarFilms from '../../components/SimilarFilms/SimilarFilms.js';

export default class Film extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            slug: '',
            title: '',
            releaseYear: '',
            description: '',
            similarFilms: [],
            isNotFound: false
        }
    }

    componentDidMount() {
        const { match } = this.props;
        const slug = match.params.slug;
        const film = getOneFilm(slug);
        if (film === null)
            this.setState({ isNotFound: true })
        else {
            this.setState({
                slug: film.slug,
                title: film.title,
                releaseYear: film.releaseYear,
                description: film.description,
                similarFilms: film.similarFilms
            });
        }
    }
    
    componentDidUpdate(previousProps, previousState) {
        const { match } = this.props;
        const newSlug = match.params.slug;
        const newFilm = getOneFilm(newSlug);
        console.log(newSlug)
        console.log(this.state.slug)
        if(this.state.slug !== newSlug){
            this.setState({
                slug: newFilm.slug,
                title: newFilm.title,
                releaseYear: newFilm.releaseYear,
                description: newFilm.description,
                similarFilms: newFilm.similarFilms
            });
        }
    }

    render() {
        const { title, releaseYear, description, similarFilms, isNotFound } = this.state;

        if (isNotFound)
            return (<h1>Désolé, nous n'avons pas trouvé votre film...</h1>)

        return (
            <section className="content">
                <h1>{title}</h1>
                <h3>{releaseYear}</h3>
                <p>{description}</p>
                <h2>Films similaire :</h2>
                {
                    similarFilms.length === 0 ?
                    (<h1>Aucun films similaire trouvés...</h1>)
                    :
                    similarFilms.map((film, i) => {
                        return (
                            <SimilarFilms key={i} slug={film.slug} title={film.title} releaseYear={film.releaseYear} onClick={this.handleClick} />
                        );
                    })
                }
            </section>
        )
    }
}