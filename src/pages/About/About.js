import React from 'react';
import FilmPreview from '../../components/FilmPreview/FilmPreview';
import {getOneFilm} from '../../api.js'

export default class About extends React.Component {
    render() {
        const film = getOneFilm('pulp-fiction');
        return (
            <div>
                <h1>About</h1>
                <p style={{marginLeft: '20%'}}>Mon film favoris est pulp fiction parce ue blablablaabla </p>
                <FilmPreview slug={film.slug} title={film.title} releaseYear={film.releaseYear} />
            </div>
        )
    }
}