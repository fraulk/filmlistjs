import React from 'react';
import { getAllFilms } from '../../api.js'
import FilmList from '../../components/FilmList/FilmList.js';

export default class Home extends React.Component {
  
  render() {
    const films = getAllFilms();
    return (
      <div>
        <h1>Tous nos films</h1>
        <div>
          <FilmList films={films} />
        </div>
      </div>
    );
  }
}
