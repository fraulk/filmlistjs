const meteo = {
    day: '',
    temperature: '',
    weather: '',
    
    setDay: (day) => {
        let days = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche'];
        if (days.includes(day))
            this.day = day;
        else
            this.day = 'NaN';

    },
    
    setTemperature: (temp) => {
        this.temperature = temp;
    },
    
    setWeather: (weather) => {
        this.weather = weather;
    },
    
    sayMeteo: () => {
        console.log('Bonjour, nous sommes ' + this.day + ', il fait ' + this.temperature + '°C et le temps est ' + this.weather);
    }
}

// Exemple de code pour vos tests:
meteo.setDay('lundi');
meteo.setTemperature(21.3);
meteo.setWeather('nuageux');
meteo.sayMeteo(); // --> Bonjour, nous sommes lundi, il fait 21.3ºC et le temps est nuageux.