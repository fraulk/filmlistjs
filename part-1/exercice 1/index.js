function add(a, b) {
    return a + b;
}
function sub(a, b) {
    return a - b;
}
function mult(a, b) {
    return a * b;
}
function div(a, b) {
    return a / b;
}

console.log(add(1, 2));
console.log(sub(1, 2));
console.log(mult(1, 2));
console.log(div(1, 2));